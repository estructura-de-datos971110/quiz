/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 *
 * @author profesor
 */
public class ListaS<T> {

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null;
    }

    public void insertarInicio(T objeto) {
        Nodo<T> nuevo = new Nodo(objeto, this.cabeza);
        this.cabeza = nuevo;
        size++;
    }

    public T get(int i) {
        return this.getPos(i).getInfo();
    }

    public void set(int i, T elemento) {
        this.getPos(i).setInfo(elemento);
    }

    public T remove(int i) {
        this.validarPos(i);
        // Caso 1: Borrar la cabeza

        Nodo<T> b = null;
        Nodo<T> anterior;
        if (i == 0) {
            b = this.cabeza;
            this.cabeza = b.getSig();
        } else {
            anterior = this.getPos(i - 1);
            b = anterior.getSig();
            anterior.setSig(b.getSig());
        }
        b.setSig(null);
        this.size--;
        return b.getInfo();
    }

    public boolean esVacio() {
        return this.cabeza == null;
    }

    public int getSize() {
        return size;

    }

    public String toString() {
        String msg = "cab<>";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.getInfo().toString() + "->";
        }
        return msg + "null";
    }

    public boolean contains(T objeto) {
        if (objeto == null || this.esVacio()) {
            throw new RuntimeException("no puede buscar un elemento");
        }
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            if (i.getInfo().equals(objeto)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    public void insertarFin(T objeto) {
        if (this.esVacio()) {
            this.insertarInicio(objeto);
        } else {
            Nodo<T> nuevo = new Nodo(objeto, null);
            Nodo<T> ultimo = this.getPos(this.size - 1);
            ultimo.setSig(nuevo);
            size++;
        }
    }

    public void virus(T info, int n) {

    }

    private Nodo<T> getPos(int i) {
        this.validarPos(i);
        // Referenciar, no crear
        Nodo<T> pos = this.cabeza;
        for (int j = 0; j < i; j++) {
            pos = pos.getSig();
        }
        return pos;
    }

    private void validarPos(int i) {
        if (this.esVacio() || i < 0 || i >= this.size) {
            throw new RuntimeException("Indice: " + i + " fuera de Rango");
        }
    }

    // Une las listas y borra la 2
    public void concat(ListaS<T> l2) {
        if (l2.esVacio() || this.esVacio()) throw new RuntimeException("Llene las dos listas");

        Nodo<T> ultimo = this.getPos(this.size - 1);
        ultimo.setSig(l2.cabeza);
        size += l2.size;
        l2.clear();

    }

    // Inserta l2 luego de i
    public void concat(int i, ListaS<T> l2) {
        validarPos(i);
        if(this.esVacio() || l2.esVacio()) throw new RuntimeException("Llene las dos listas");

        Nodo<T> insertar = this.getPos(i-1);
        Nodo<T> siguiente = this.getPos(i);

        insertar.setSig(l2.cabeza);
        l2.getPos(l2.size-1).setSig(siguiente);;
        size += l2.size;
        l2.clear();
    }

}
