/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Docente
 */
public class Fraccionario implements Comparable<Fraccionario> {

    private float numerador;
    private float denominador;

    public Fraccionario() {
    }

    public Fraccionario(float numerador, float denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    public float getNumerador() {
        return numerador;
    }

    public void setNumerador(float numerador) {
        this.numerador = numerador;
    }

    public float getDenominador() {
        return denominador;
    }

    public void setDenominador(float denominador) {
        this.denominador = denominador;
    }

    @Override
    public String toString() {
        return numerador + "/" + denominador;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Float.floatToIntBits(this.numerador);
        hash = 53 * hash + Float.floatToIntBits(this.denominador);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fraccionario other = (Fraccionario) obj;
        return this.getDecimal() == other.getDecimal();
    }

    public float getDecimal() {
        return this.numerador / this.denominador;
    }

    @Override
    public int compareTo(Fraccionario o) {
        float rta = this.getDecimal() - o.getDecimal();
        if (rta == 0) {
            return 0;
        }
        if (rta < 0) {
            return -1;
        }
        return 1;
    }

}
